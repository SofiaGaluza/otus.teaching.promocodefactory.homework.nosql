﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IDistributedCache _distributedCache;
        const string RedisKey = "preferences";

		public PreferencesController(IRepository<Preference> preferencesRepository, IDistributedCache distributedCache)
		{
			_preferencesRepository = preferencesRepository;
			_distributedCache = distributedCache;
		}
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
	        var serialized = await _distributedCache.GetStringAsync(RedisKey);

	        if (serialized != null)
	        {
		        return Ok(JsonSerializer.Deserialize<List<PreferenceResponse>>(serialized));
	        }

            var preferences = await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            await _distributedCache.SetStringAsync(
	            key: RedisKey,
	            value: JsonSerializer.Serialize(response),
	            options: new DistributedCacheEntryOptions
	            {
		            SlidingExpiration = TimeSpan.FromSeconds(30)
	            });


			return Ok(response);
        }
    }
}